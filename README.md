Solution for agoro coding challenge implementing a basic RESTful API built in Python-flask and marshmallow library. Data is stored in a MongoDB in a separate container using dockers prebuilt MongoDB container. The API is hosted via WSGI server.

---

## Quickstart

to install and run both containers in their default config Docker compose is required:

```bash
docker compose build
docker compose up

```

This should install all required packages, create a network for both containers and run them.
The app should be accessible on the local machine via [localhost:5000](localhost:5000).
The only URL needed for all functions is [/soil_data_layers](localhost:5000/soil_data_layers). The API is self documenting using hypermedia and error codes but human readable descriptions are given when applicable.


---


## Basic Operations

* Get all soil data layers: **GET:**[/soil_data_layers](localhost:5000/soil_data_layers)
* Get a filtered list of soil data layers based on list of mukeys:  **GET:**[/soil_data_layers/?mukey=<mukey1>&mukey=<mukey2>&mukey=<mukey3>](localhost:5000/soil_data_layers)
* Get detail view of single observation: **GET:**[/soil_data_layers/<mukey>](localhost:5000/soil_data_layers)
* Import CSV file with new observations: **POST:** [/soil_data_layers](localhost:5000/soil_data_layers) *A file header is required containing a single **.csv** file.*  
**Note:** the import route only allows .csv files up to 1GB in size which is an arbitrary number just to not allow any filesize. since mukeys are used for identifying observations files without a column named mukey are rejected. Also rows that don't contain a mukey or one that is already present in the database will be omitted from import. The response contains information about how many and which observations were successfully imported.

In a production setting of course further functions could be necessary like:  
* Modify a single observation via **PUT** on [/soil_data_layers/<mukey>](localhost:5000/soil_data_layers) including a JSON body with the changes
* Delete a single observation via **DELETE** on [/soil_data_layers/<mukey>](localhost:5000/soil_data_layers)
* Delete multiple observations via **DELETE** on [/soil_data_layers/?mukey=<mukey1>&mukey=<mukey2>&mukey=<mukey3>](localhost:5000/soil_data_layers)


---


## Design Decisions

**Technologies used:**  
I decided to go with a classical REST API as only big imports and requests for **all** fields of observations were required. So none of GraphQLs filtering was needed and RESTapis are better for making a deliberate choice between updating a resource and creating it, which might be important in this case where we use externally generated IDs(mukeys) for accessing data. With this solution we can also make use of HTTP caching.  
The API is hosted via WSGSI using 5 threads as this is a well established solution and I don't know the expected load.  
The persistance layer is supplied via a MongoDB. I Chose this as the data is a simple collection of observations with no relations to one another. As we don't know the load or expandability requirements I chose MongoDB because it has proven to scale exceptionally well and serve very big requests often fater than classical relational databases. It is also very adaptable and useful for our case as I decided to be agnostic to import values.  
The prebuilt docker MongoDB image serves a great purpose here as it makes deployment easy and fast. With very little configuration it could also be also be substituted with an externally hosted database of the same or similar type.  

**REST:**  
The API makes full use of the REST paradigm. Resources are identified with discrete URLs/URIs and the type of operation is specified via HTTP request methods. Errors are reported with appropriate status codes and all successful calls get responses containing hypermedia links to all related operations. The API is self documenting and doesn't necessarily need to be versioned if the clients make full use of the REST paradigm.  

**How data is handled:**  
Under the assumption that mukeys are supposed to be unique identifiers, the database is indexed with mukeys to increase performance. With this in mind I also disallow multiple entries to the database using the same mukey. observations from csv files that feature a mukey already in the database or an invalid one are omitted from import. Files that don't contain a mukey column are rejected. All of this behavior could of course be different if mukeys were not supposed to be unique or new data was supposed to replace old data with the same mukey. Observations could also internally be represented with an internal unique ID if multiple entries with the same mukey should be allowed but that would make filtering for mukeys rather useless.

One other decision was to not make assumptions on the data and what columns it might contain. I don't validate imported observations for missing values as the rest of the observation might still be required and I also don't omit observations that have no value other than the mukey as, depending on the use keys, their existance still provides some metadata that might be required.  
For expandability purposes the columns in the database are directly derived from the csv file. We might later get a file with fewer or more or completely different columns and that might still be relevant to whichever project is using the API so data is imported and served dynamically. MongoDB is a very good choice for this usecase.  
I didn't know the encoding used in the column names but it suggests that this is used for some column specific properties.

**General stuff**  
The config is mostly inside the app.py as well as app.ini but some bits of database specific config as well as collection names and solution specific keywords are sometimes right where they apply. Those would of course be put in a config file for a production environment to make the code more generic and easier to maintain.  
In a production environment I would also suggest using Openapi code to standartize as well as document the API.  
The drainage category could also be normalized to save on redundancy and make it faster to search if it was supposed to be a searchable parameter. 
Depending on the loads we expect we could also add a load balancer and run multiple instances or make use of message queues to improve caching, batch processing or working with other possible services. A bigger project would also justify using Terraform to streamline infrastructure as well as using an actual CI/CD strategy to make deployment faster.

## Responses

* **GET** [/soil_data_layers](localhost:5000/soil_data_layers):
```python
{
  "_links": {  },		#links to this resource as well as related operations
  "data": {  },			#data part cotaining all fields of this observation
}

```

* **GET:**[/soil_data_layers/?mukey=<mukey1>&mukey=<mukey2>&mukey=<mukey3>](localhost:5000/soil_data_layers):
```python
{
  "_links": {  },		#links to this resource as well as related operations
  "collection": {  },	#collection part containing the requested observation objects
  "length": ,		#number of observations in collection
}

```

* **POST:**[/soil_data_layers/>](localhost:5000/soil_data_layers):
```python
{
  "_links": {  },					#links to this resource as well as related operations
  
  "importResults": {		
      "filename": ,					#name of uploaded file
      "importedCount": ,			#number of successfully imported observations
      "importedMukeys": [], 		#list of mukeys of all successfully imported observations
      "soil_data_layers_total": , 	#number of observations in database after import
      "success": ,					#import outcome
    }		
}

```




