from collections import namedtuple
import pandas as pd
from pymongo import errors
import json


ALLOWED_EXTENSIONS = {'csv', }

#check if file in list of allowed extensions
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

"""
import local csv file via pandas into collection
returns a dict of import stats
"""
def mongoimport(csv_path, db, coll_name):
    coll = db[coll_name]

    #read csv file into dataframe
    data = pd.read_csv(csv_path)

    #enforce mukey as it is the parameter by which all data is accessed
    if 'mukey' not in data.columns:
        raise ValueError
    
    #make sure the database indexes by mukeys to improve performance
    #also make sure mukeys are unique
    coll.create_index("mukey", unique=True)

    #format data for import. remove rows without a value for mukey
    data['mukey'].replace(r'^s*$', float('NaN'))
    data[data['mukey'].notna()]
    payload = json.loads(data.to_json(orient='records'))

    count = coll.count()

    try:
        coll.insert_many(payload,ordered=False)

    except errors.BulkWriteError as e:
        #collect list of mukeys of all entries that couldn't be imported due to errors (most likely duplicate keys)
        errorMukeys = [d["keyValue"]["mukey"] for d in  e.details["writeErrors"]]
        #remove not imported values from dataframe for later reporting
        data = data[~data.mukey.isin(errorMukeys)]
    
    #get numer of imported values from new collection size in database
    count = coll.count() - count
    
    return {
            "success" : True,
            "importedCount" : count,
            "importedMukeys" : data.mukey.to_string(index=False),
            "soil_data_layers_total" : coll.count()
        }


#wrapper to get values from database into right output format in restful api
def dataObjectWrapper(obj):
    """
    Wraps a dict of values into the api defined return standard
    """
    try:
        del obj["_id"]
    except KeyError:
        pass
    obj = {"data" : obj}
    return namedtuple("Data", obj.keys())(*obj.values())

