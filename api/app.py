from flask import Flask
from routes import Routes
from schemas import Schemas
from db import mongo

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['MONGO_URI'] = "mongodb://root:pass12345@mongodb:27017"  #onfig['PROD']['DB_URI']
app.config['MAX_CONTENT_LENGTH'] = 1 * 1000 * 1000 * 1000
app.config['UPLOAD_FOLDER'] = "static/uploads"

mongo.init_app(app)
Routes(app)
Schemas(app)

