import os
from posixpath import dirname, realpath
from uuid import uuid4
from flask import Response
from flask.globals import request

from werkzeug.utils import secure_filename
from helpers import dataObjectWrapper, allowed_file, mongoimport
from db import mongo




class Routes:
    

    def __init__(self,app):
        self.NotFoundError = Response("{'message':'the resource corresponding to the requested URI was not found'}", status=404, mimetype='application/json')
        self.UnsupportedError = Response("{'message':'this file type is not supported.'}", status=415, mimetype='application/json')
        self.TooLargeError = Response("{'message':'file too large'}", status=413, mimetype='application/json')
        self.UnprocessableError = Response("{'message':'file not processable'}", status=422, mimetype='application/json')
        self.NoFileError = Response("{'message':'no file in request found'}", status=422, mimetype='application/json')
        self.InternalError = Response("{'message':'an internal error occurred'}", status=500, mimetype='application/json')
        self.InvalidInputError = Response("{'message':'Input parameter is invalid'}", status=400, mimetype='application/json')

        """
        @app.route("/")
        def hello_world():

        @app.route("/deleteall")
        def deleteall():
            mongo.db.soil_data_layers.remove()
            return "<p>deleted everything</p>"
        """


        @app.route("/soil_data_layers/<mukey>", methods=['GET'])
        def singleDataLayer(mukey):

            #if uri doesn't contain a valid mukey at the end a non valid resource was requested
            try:
                mukey = int(mukey)
            except ValueError:
                return self.NotFoundError

            #get corresponding data entry
            data  = mongo.db.soil_data_layers.find_one({'mukey' : mukey})

            #if nothing is found return not found response
            if data == None:
                return self.NotFoundError

            return app.schemas.sdlSchema(dataObjectWrapper(data))

        @app.route("/soil_data_layers/", methods=['GET', 'POST'])
        def soilDataLayers():
            """
            POST REQUEST 
            For Uploading files in CSV Format
            """
            if request.method == 'POST' :
                 # check if the post request has the file part
                if len(request.files) < 1:
                    return self.NoFileError
                file = list(request.files.values())[0]
                # check if file part is empty
                if file.filename == '':
                    return self.UnprocessableError 
                #check for allowed file type and get upload path
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    extension = filename.rsplit('.', 1)[1].lower()
                    path = os.path.join(dirname(realpath(__file__)), app.config['UPLOAD_FOLDER'])

                    #create upload path if not yet created
                    if not os.path.exists(path):
                        os.makedirs(path)
                    
                    #generate random unique file name to avoid security issues and two files being written having the same name
                    filepath = os.path.join(path, str(uuid4()) + "." + extension)

                    try:
                        file.save(filepath)
                    except OSError as e:
                        return self.InternalError

                    #try import 
                    resultStats = mongoimport(filepath,mongo.db, "soil_data_layers")
                    resultStats["filename"] = filename

                    #delete file after import
                    os.remove(filepath)
                    return app.schemas.sdlImportSchema(resultStats)

                else:
                    return self.UnsupportedError

            #GET REQUEST
            #for getting all of the soil data layers or
            #a list filtered via the repeatable "mukey" get parameter
            else:
                mukeys = request.args.getlist("mukey")
                coll = []

                #if mukey parameter is not used return all
                if mukeys == None or len(mukeys) < 1:
                    coll = mongo.db.soil_data_layers.find({})

                else:
                    #turn mukeys into list
                    try:
                        mukeys = list(map(int,mukeys))
                    #return error if invalid mukey parameter was given
                    except ValueError:
                        return self.InvalidInputError
                    #get all entries corresponding to one of the mukeys each
                    coll = mongo.db.soil_data_layers.find({'mukey': {"$in": mukeys}})
                

                #ormat data for output
                coll = map(dataObjectWrapper, coll)

                return app.schemas.sdlCollSchema(coll)