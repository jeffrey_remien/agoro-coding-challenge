from flask_marshmallow import Marshmallow
from collections import namedtuple


class Schemas:
    def __init__(self, app):
        self.ma = Marshmallow(app)

        #wrapper to wrap collection items into format for restful api
        def collectionBody(col):
            collen = len(col)
            col = {
                "collectionData" : col,
                "length" : collen,
                }
            return namedtuple("Collection", col.keys())(*col.values())

        """
        schema for single intry in soil data layers
        """
        class SoilDataLayerSchema(self.ma.Schema):
            class Meta:
                # Fields to expose
                fields = ("data","_links")

            # Smart hyperlinking
            _links = self.ma.Hyperlinks(
                {
                    "self": {
                        "href": self.ma.URLFor("singleDataLayer", values=dict(mukey="<data.mukey>")),
                        "method" : "GET",
                    },
                    "collection": {
                        "href": self.ma.URLFor("soilDataLayers"),
                        "parameters": {
                            "mukey": "repeatable parameter to filter collection for mukeys"
                        },
                        "method" : "GET",
                    },
                }
            )

        """
        schema for multiple entries in soil data layers
        """
        class SoilDataLayerCollectionSchema(self.ma.Schema):
            class Meta:
                # Fields to expose
                fields = ("collectionData","length","importResults","_links")

            _links = self.ma.Hyperlinks(
                    {
                        "self" : {
                            "href": self.ma.URLFor("soilDataLayers"),
                            "parameters": {
                                "mukeys": {
                                    "description": "filter collection for list of mukeys",
                                }
                            },
                            "method" : "GET",
                        },
                        "importCSV": {
                            "href": self.ma.URLFor("soilDataLayers"),

                            "method" : "POST",
                        }
                    }
                
                )


        #transformations required to imject necessary parameters for restful api in all schemas
        sdlCollLinks = SoilDataLayerCollectionSchema().dump
        sdlManySchema = SoilDataLayerSchema(many=True).dump
        def sdlImportSchema(res): 
            res = {"importResults" : res}
            return sdlCollLinks(namedtuple("importResults", res.keys())(*res.values()))

        def sdlCollSchema(coll):
            return sdlCollLinks(collectionBody(sdlManySchema(coll)))

       
        schemas ={
            "sdlSchema" : SoilDataLayerSchema().dump,
            "sdlCollSchema" : sdlCollSchema,
            "sdlImportSchema" : sdlImportSchema,
        }

        #return schemas and schema helpers and add them as parameter to app so they can be acessed elsewhere
        app.schemas = namedtuple("Schemas", schemas.keys())(*schemas.values())